from django.test import TestCase, Client

# Create your tests here.
class test_story_8_ajax(TestCase):
    def test_halaman_bisa_dibuka(self):
        response = Client().get('/story8')
        self.assertEqual(response.status_code, 200)
    
    def test_halaman_menggunakan_template(self):
        response = Client().get('/story8')
        self.assertTemplateUsed(response, 'buku/buku.html')
        self.assertTemplateUsed(response, 'base.html')

    def test_data_google_books_melalui_views(self):
        response = Client().get('/data', data={'q':'test'})
        self.assertEqual(response.status_code, 200)