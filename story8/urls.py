from django.urls import path
from . import views

urlpatterns = [
    path('story8', views.buku, name='buku'),
    path('data', views.data, name='data'),
]