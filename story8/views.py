from django.shortcuts import render
from django.http import JsonResponse
import requests

# Create your views here.
def buku(request):
    return render(request, 'buku/buku.html')

def data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    return JsonResponse(ret.json(), safe=False)