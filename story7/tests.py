from django.test import TestCase, Client

# Create your tests here.
class Story7Test(TestCase):
    def test_halaman_muncul(self):
        response = Client().get('/story7')
        self.assertEqual(response.status_code, 200)

    def test_menggunakan_template(self):
        response = Client().get('/story7')
        self.assertTemplateUsed(response, 'accordion/home.html')
        self.assertTemplateUsed(response, 'base.html')