from django.shortcuts import render, redirect
from .models import Kegiatan, Peserta
from .forms import KegiatanForm, PesertaForm

# Create your views here.
def kegiatan(request):
    list_kegiatan = Kegiatan.objects.all()
    dict_peserta = {}
    for kegiatan in list_kegiatan:
        dict_peserta[kegiatan] = Peserta.objects.filter(kegiatan = kegiatan)

    response = {
        'peserta_list': dict_peserta
    }
    return render(request, 'kegiatan/kegiatan.html', response)

def add_kegiatan(request):
    if request.method == "POST":
        form = KegiatanForm(request.POST)
        if form.is_valid():
            kegiatan = form.save()
            kegiatan.save()
            return redirect('kegiatan')
    else:
        form = KegiatanForm()
    return render(request, 'kegiatan/kegiatan_form.html', {'form':form})

def add_peserta(request, id):
    if request.method == "POST":
        form = PesertaForm(request.POST)
        if form.is_valid():
            peserta = form.save(commit=False)
            peserta.kegiatan = Kegiatan.objects.get(id=id)
            peserta.save()
            return redirect('kegiatan')
    else:
        form = PesertaForm()
    return render(request, 'kegiatan/peserta_form.html', {'form':form})