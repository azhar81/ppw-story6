from django.test import TestCase, Client, LiveServerTestCase, tag
from django.urls import resolve
from .models import Kegiatan, Peserta
from .views import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class TestStory6(TestCase):
    def test_url_berhasil(self):
        response = Client().get('/story6')
        self.assertEqual(response.status_code, 200)

    def test_url_kegiatan_menggunakan_template1(self):
        response = Client().get('/story6')
        self.assertTemplateUsed(response, 'kegiatan/kegiatan.html')

    def test_url_kegiatan_menggunakan_template2(self):
        response = Client().get('/story6')
        self.assertTemplateUsed(response, 'base.html')

    def test_apakah_ada_model_kegiatan(self):
        kegiatan = Kegiatan(nama_kegiatan='testkegiatan')
        kegiatan.save()
        hitung_jumlah_kegiatan = Kegiatan.objects.all().count()
        self.assertEqual(hitung_jumlah_kegiatan,1)
    
    def test_apakah_model_peserta_dan_kegiatan_terdapat_relationship(self):
        kegiatan = Kegiatan(nama_kegiatan='testkegiatan')
        kegiatan.save()
        peserta = Peserta(nama_peserta='testpeserta', kegiatan=kegiatan)
        peserta.save()
        hitung_jumlah_peserta_pada_kegiatan = Peserta.objects.filter(kegiatan=kegiatan).count()
        self.assertEqual(hitung_jumlah_peserta_pada_kegiatan,1)

    def test_apakah_data_muncul_pada_halaman_saat_menambahkan_kegiatan(self):
        kegiatan = Kegiatan(nama_kegiatan='testkegiatan')
        kegiatan.save()
        response = Client().get('/story6')
        html_kembalian = response.content.decode('utf8')
        self.assertIn('testkegiatan', html_kembalian)

    def test_apakah_peserta_muncul_saat_menambahkan_peserta(self):
        kegiatan = Kegiatan(nama_kegiatan='testkegiatan')
        kegiatan.save()
        peserta = Peserta(nama_peserta='testpeserta', kegiatan=kegiatan)
        peserta.save()
        response = Client().get('/story6')
        html_kembalian = response.content.decode('utf8')
        self.assertIn('testpeserta', html_kembalian)
    
    def test_apakah_ada_text_yang_seharusnya_ada(self):
        response = Client().get('/story6')
        html_kembalian = response.content.decode('utf8')
        self.assertIn('Kegiatan Azhar', html_kembalian)

    def test_apakah_ada_text_yang_seharusnya_tidak_ada(self):
        response = Client().get('/story6')
        html_kembalian = response.content.decode('utf8')
        self.assertNotIn('ini seharusnya tidak ada', html_kembalian)

    def test_using_the_right_function_on_main_page(self):
        found = resolve('/story6')
        self.assertEqual(found.func, kegiatan)

    def test_model_kegiatan_mengembalikan_nama_nya(self):
        kegiatan = str(Kegiatan(nama_kegiatan='testkegiatan'))
        self.assertEqual('testkegiatan', kegiatan)
        
    def test_model_peserta_mengembalikan_nama_nya(self):
        kegiatan = Kegiatan(nama_kegiatan='testkegiatan')
        kegiatan.save()
        peserta = str(Peserta(nama_peserta='testpeserta', kegiatan=kegiatan))
        self.assertEqual('testpeserta', peserta)

@tag('functional')
class Lab6FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome(options=options)

    def tearDown(self):
        # hapus kegiatan yang dibuat
        Kegiatan.objects.get(nama_kegiatan='test_kegiatan_yeah').delete()

        self.selenium.quit()
        super(Lab6FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get(f'{self.live_server_url}/story6')

        # Pembuatan kegiatan
        # find the form element
        add_kegiatan_button = selenium.find_element_by_id('add_kegiatan_button')
        # click the button
        add_kegiatan_button.click()
        # find input element
        kegiatan_input = selenium.find_element_by_id('id_nama_kegiatan')
        # do the things
        kegiatan_input.send_keys('test_kegiatan_yeah')
        kegiatan_input.send_keys(Keys.RETURN)
        
        # Pembuatan peserta
        # generasi kode id button add peserta pada kegiatan
        id_kegiatan = Kegiatan.objects.get(nama_kegiatan='test_kegiatan_yeah').id
        id_tombol_add_peserta = 'add_peserta_' + str(id_kegiatan)
        # find button by id
        tombol_add_peserta = selenium.find_element_by_id(id_tombol_add_peserta)
        tombol_add_peserta.click()
        # find input element
        input_nama_peserta = selenium.find_element_by_id('id_nama_peserta')
        # masukkan input dan submit
        input_nama_peserta.send_keys('test_nama_peserta')
        input_nama_peserta.send_keys(Keys.RETURN)