from django.urls import path
from . import views

#url for app
urlpatterns = [
    path('story6', views.kegiatan, name='kegiatan'),
    path('story6/add_kegiatan', views.add_kegiatan, name='add_kegiatan'),
    path('story6/add_peserta/<int:id>/', views.add_peserta, name='add_peserta'),
]
