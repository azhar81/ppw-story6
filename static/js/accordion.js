$(document).ready(function(){
    $(".isi").hide();

    $("h3").click(function() {
        $(this).parents("div.accordion").children("div.isi").toggle();
    });

    $(".move-up").click(function() {
        var curr = $(this).parents("div.accordion")
        var previous = curr.prev();
        if (previous.val() != undefined) {
            previous.before(curr);
        }
    });

    $(".move-down").click(function() {
        var curr = $(this).parents("div.accordion")
        var next = curr.next();
        if (next.val() != undefined) {
            next.after(curr);
        }
    });
});