from django.urls import path, include
from . import views

urlpatterns = [
    path('story9', views.landing_login, name='landing_login'),
    path('story9/register/', views.register_view, name='register'),
    path('story9/', include('django.contrib.auth.urls')),
]