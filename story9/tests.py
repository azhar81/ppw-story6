from django.test import TestCase, Client, tag, LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.auth.models import User

# Create your tests here.
class TestStory9Auth(TestCase):
    def test_halaman_bisa_dibuka(self):
        response = Client().get('/story9')
        self.assertEqual(response.status_code, 200)

    def test_halaman_menggunakan_template(self):
        response = Client().get('/story9')
        self.assertTemplateUsed(response, 'story9/landing.html')
        self.assertTemplateUsed(response, 'base.html')

@tag('functional')
class Story9FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome(options=options)

    def tearDown(self):
        # hapus kegiatan yang dibuat
        User.objects.get(username='@test_username@').delete()

        self.selenium.quit()
        super(Story9FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get(f'{self.live_server_url}/story9/register')

        # find input element
        username_input = selenium.find_element_by_id('id_username')
        password1_input = selenium.find_element_by_id('id_password1')
        password2_input = selenium.find_element_by_id('id_password2')
        # do the things
        username_input.send_keys('@test_username@')
        password1_input.send_keys('@test_password@')
        password2_input.send_keys('@test_password@')
        username_input.send_keys(Keys.RETURN)