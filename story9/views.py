from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import login
from django.contrib.auth.forms import UserCreationForm

# Create your views here.
def landing_login(request):
    return render(request, 'story9/landing.html')

def register_view(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('landing_login')
    else:
        form = UserCreationForm()
    return render(request, 'registration/register.html', {'form':form})
