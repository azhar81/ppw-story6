from django.urls import path
from . import views

#url for app
urlpatterns = [
    path('', views.landing, name='landing'),
]
