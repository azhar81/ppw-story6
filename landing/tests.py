from django.test import TestCase, Client

# Create your tests here.
class TestMain(TestCase):
    def test_url_berhasil(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_template(self):
        response = Client().get('/')
        self.assertTemplateUsed('landing.html')
        self.assertTemplateUsed('base.html')